export type ContentfulTag = {
  name: string;
  slug: string;
};
